#include <iostream>
#include <yaml-cpp/yaml.h>

using namespace std;

int main()
{
    cout << "yaml load from default" << endl;
    YAML::Node config = YAML::LoadFile("/home/cat/dev/self-balance-proto-mi9/config/default.yaml");
    if (config["floatvalue"])
    {
        cout << "floatValue:" << config["floatvalue"].as<float>() << endl;
    }
    if (config["stringvalue"])
    {
        cout << "stringvalue:" << config["stringvalue"].as<string>() << endl;
    }
    if (config["boolvalue"])
    {
        cout << "boolvalue:" << config["boolvalue"].as<bool>() << endl;
    }
    if (config["structvalue"])
    {
        if (config["structvalue"]["index"])
        {
            cout << "index:" << config["structvalue"]["index"].as<int>() << endl;
        }
        if (config["structvalue"]["value"])
        {
            cout << "value:" << config["structvalue"]["value"].as<float>() << endl;
        }
    }
    return 0;
}