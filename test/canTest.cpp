#include <can.hpp>
#include <cstring>
#include <iostream>

using namespace std;
const uint32_t canID = 0x70;
int main()
{
    cout << "Can test start" << endl;
    Can can;
    if (!can.Open("can0"))
    {
        cout << "Error open Can0" << endl;
    }

    can_frame frame;
    frame.can_id = canID;
    frame.can_dlc = 2;
    frame.data[0] = 0x0f;
    frame.data[1] = 0x00;
    bool sendB = can.SendFrame(frame);
    cout << (sendB ? "Send success" : "Send fail") << endl;

    auto backFrame = can.GetFrame();
    backFrame = can.GetFrame(); // why loopback can not be disabled?
    cout << (backFrame.can_id == canID ? "ID return right." : "ID return fail.") << endl;
    cout << (backFrame.can_dlc == 4 ? "length return right." : "length return fail.") << endl;
    cout << "data is:";
    for (int i = 0; i < backFrame.can_dlc; i++)
    {
        cout << std::hex << std::showbase << backFrame.data[i];
    }
    cout << endl;
    uint32_t result;
    memcpy(&result, backFrame.data + 2, 2);
    cout << "ErrorCode is:" << result << endl;
    return 0;
}