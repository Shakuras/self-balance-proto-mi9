#include <messageManager.hpp>
#include <cstring>
#include <iostream>

using namespace std;

struct Mes
{
    char info[64];
};

int main()
{
    MessageManager<Mes> manager;
    if (!manager.Open("/tmp", 0))
    {
        return -1;
    }

    cout << "Writer start" << endl;
    while (true)
    {
        string message;
        cin >> message;
        if (message == "exit")
        {
            break;
        }
        Mes mes;
        memset(mes.info, 0, sizeof(mes.info));
        strcpy(mes.info, message.c_str());
        cout << "send: " << mes.info << endl;
        if (!manager.Send(5, mes))
        {
            cout << "send error" << endl;
        }
    }
    manager.RemoveMessageQueue();
    return 0;
}