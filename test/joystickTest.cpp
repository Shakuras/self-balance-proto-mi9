#include <joystick.hpp>
#include "JsConfig.hpp"

#include <iostream>
using namespace std;

int main()
{
    JoyStick js;
    if (!js.InitDevice("/dev/input/js0"))
    {
        cout << "Failed to initialize" << endl;
    }

    while (true)
    {
        js.CheckDeviceInput();
        system("clear");
        cout << "Aix: " << endl;
        for (size_t i = 0; i < CustormJoyStickAixCount; i++)
        {
            int index = CustormAixNumbers[i];
            cout << index << " : " << js.GetAix(index) << '\t';
        }
        cout << endl;
        cout << "Button: " << endl;
        for (size_t i = 0; i < CustormJoyStickButtonCount; i++)
        {
            int index = CustormButtonNumbers[i];
            cout << index << " : " << js.GetButton(index) << '\t';
        }
        cout << endl;
    }
    return 0;
}