#include <messageManager.hpp>
#include <iostream>
#include <string.h>

using namespace std;

struct Mes
{
    char info[64];
};

int main()
{
    MessageManager<Mes> manager;
    if (!manager.Open("/tmp", 0))
    {
        return -1;
    }

    cout << "Reader start" << endl;
    while (true)
    {
        Mes mes;
        memset(&mes, 0, sizeof(mes));
        if (manager.Receive(5, mes))
        {
            cout << mes.info << endl;
        }
    }
    manager.RemoveMessageQueue();
    return 0;
}