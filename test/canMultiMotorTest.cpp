#include <iostream>
#include <motor_CAN.hpp>

using namespace std;

int main()
{
    cout << "Start Motor Can Test" << endl;

    auto server = make_shared<CanServer>();
    if (!server->StartServer("can0"))
    {
        cout << "Open Can Server fail." << endl;
        return -1;
    }

    Motor_Can motorL;
    MotorConfig cfgL{.address = 0x70, .poleCount = 15};
    if (!motorL.InitDevice(server, cfgL))
    {
        cout << "Init motor fail" << endl;
        return -1;
    }
    Motor_Can motorR;
    MotorConfig cfgR{.address = 0x1c, .poleCount = 15};
    if (!motorR.InitDevice(server, cfgR))
    {
        cout << "Init motor fail" << endl;
        return -1;
    }

    while (true)
    {
        auto start = chrono::steady_clock::now();

        motorL.RequestRPM();
        motorL.SendHeartBeat();
        // motorL.SetCurrent(1000);
        motorR.RequestRPM();
        motorR.SendHeartBeat();
        // motorR.SetCurrent(1000);

        auto now = chrono::steady_clock::now();
        int us = chrono::duration_cast<chrono::microseconds>(now - start).count();

        cout << "rpm:" << motorL.GetRPM() << '-' << motorR.GetRPM() << endl;
        cout << "fps:" << 1000000 / us << endl;
        this_thread::sleep_for(chrono::milliseconds(50));
    }
    return 0;
}