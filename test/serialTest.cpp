#include <serial.hpp>
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
    cout << "serial driver test" << endl;

    Serial serial;
    string dev = "/dev/pts/4";
    if (!serial.Open(dev, 9600))
    {
        cout << "open com fail" << endl;
        return -1;
    }
    while (true)
    {
        vector<char> receive = serial.Read();
        if (!receive.size() <= 0)
        {
            for (int i = 0; i < receive.size(); i++)
            {
                cout << receive[i];
            }
            cout << endl;
        }
        string mes;
        cin >> mes;
        vector<char> mesvec;
        for (int i = 0; i < mes.length(); i++)
        {
            mesvec.push_back(mes[i]);
        }

        serial.Write(mesvec);
    }
    serial.Close();
    return 0;
}
// socat -d -d pty,raw,echo=0,link=$HOME/vstty0 pty,raw,echo=0,link=$HOME/vstty1
