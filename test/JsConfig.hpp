#pragma once

const int CustormJoyStickAixCount = 8;
const int CustormJoyStickButtonCount = 10;
const int CustormAixNumbers[CustormJoyStickAixCount] = {0, 1, 2, 3, 4, 5, 6, 7};
const int CustormButtonNumbers[CustormJoyStickButtonCount] = {0, 1, 4, 3, 6, 7, 8, 9, 10, 11};

enum CustormJoyStickAixNumber
{
    AixLeftHorizental = 0,
    AixLeftVertical = 1,
    AixRightHorizental = 2,
    AixRightVertical = 3,
    LeftTrigget = 5,
    RightTrigger = 4,
    FourKeyLeftHorizental = 6,
    FourKeyLeftVertical = 7,
};

enum CustormJoyStickButtonNumber
{
    Select = 10,
    Start = 11,
    LeftTriger1 = 6,
    LeftTriger2 = 8,
    RightTrigger1 = 7,
    RightTrigger2 = 9,
    ButtonX = 3,
    ButtonY = 4,
    ButtonB = 1,
    ButtonA = 0
};
