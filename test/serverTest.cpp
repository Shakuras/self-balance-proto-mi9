#include <chrono>
#include <cstdio>
#include <netServer.hpp>
#include <iostream>
#include <thread>

using namespace std;

int main()
{
    NetServer server;
    if (server.Open("192.168.10.100", 5002, 2))
    {
        cout << "Creat server Success" << endl;
    }

    int i = 0;
    while (true)
    {
        i++;
        server.SetValue("index", i);
        float value;
        if (server.GetValue("move", value))
        {
            printf("move value is %f \n", value);
        }
        // cout << "Net Server is runing..." << endl;
        this_thread::sleep_for(chrono::milliseconds(100));
    }

    server.Close();

    return 0;
}