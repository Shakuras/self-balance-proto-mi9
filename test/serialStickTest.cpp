#include <serialStick.hpp>
#include <iostream>
#include <thread>

using namespace std;

int main(int argc, char **argv)
{
    cout << "serial stick driver test" << endl;

    SerialStick stick;
    if (!stick.InitDevice("/dev/ttyS6", 115200))
    {
        cout << "open Stick Serial fail" << endl;
        return -1;
    }
    while (true)
    {
        stick.ReadData();
        for (int i = 0; i < 4; i++)
        {
            float value = stick.GetAix(i);
            std::cout << value << '\t';
        }
        std::cout << std::endl;
        this_thread::sleep_for(chrono::milliseconds(100));
    }
    return 0;
}