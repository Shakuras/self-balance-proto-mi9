#pragma once
#include <string>
#include <vector>

#define BUFFERSIZE 1024

class Serial
{
  public:
    Serial();
    ~Serial();
    bool Open(std::string device, int rate);
    bool Close();
    std::vector<char> Read();
    bool Write(std::vector<char> message);

  private:
    int fd;
    std::vector<char> recBuffer;
};