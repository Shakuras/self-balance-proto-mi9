#pragma once

#include <map>
#include <string>

class JoyStick
{
  public:
    bool InitDevice(std::string dev);
    /// @brief
    /// @param number aix number
    /// @return in range [-1,1]
    float GetAix(int number);
    bool GetButton(int number);
    void CheckDeviceInput();
    ~JoyStick();

  private:
    struct JoyStickState
    {
        std::map<int, float> aixsValue;
        std::map<int, bool> buttonValue;
    };
    JoyStickState state;
    int fd;
};
