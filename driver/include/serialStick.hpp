#pragma once
#include <string>
#include "serial.hpp"
#include <vector>
class SerialStick
{
  public:
    bool InitDevice(std::string dev, int baudRate);
    void ReadData();
    float GetAix(int aix);

  private:
    Serial serial;
    float aixValues[4]{0, 0, 0, 0};
    std::vector<char> buff;
    int currentIndex = 0;
};