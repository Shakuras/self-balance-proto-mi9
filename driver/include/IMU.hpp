#pragma once

#include "serial.hpp"
#include <sstream>
#include <string>

struct IMUState
{
    float acc[3]{0, 0, 0};
    float gyro[3]{0, 0, 0};
    float angle[3]{0, 0, 0};

    std::string PrintState()
    {
        using namespace std;
        ostringstream info;
        info << "acc:" << acc[0] << ", " << acc[1] << ", " << acc[2] << '\n';
        info << "gyro" << gyro[0] << ", " << gyro[1] << ", " << gyro[2] << '\n';
        info << "angle" << angle[0] << ", " << angle[1] << ", " << angle[2] << '\n';
        return info.str();
    }
};

class IMU
{
  public:
    bool InitDevice(std::string dev, int baudrate);
    IMUState GetState();

  private:
    volatile char dataUpdateFlag = 0;
    Serial serial;
    void SensorDataUpdata(uint32_t uiReg, uint32_t uiRegNum);
};
