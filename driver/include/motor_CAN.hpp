#pragma once
#include "canServer.hpp"
#include "motor.hpp"
#include <memory>
#include <atomic>

class Motor_Can
{
  public:
    bool InitDevice(std::shared_ptr<CanServer> canBusInst, MotorConfig cfg);
    float GetRPM();
    bool RequestRPM();
    bool SetRPM(float value);
    bool SetPWM(float precent);
    bool SetCurrent(float mA);
    bool SendHeartBeat();
    Motor_Can();
    ~Motor_Can();

  private:
    std::shared_ptr<CanServer> bus;
    MotorConfig cfg;
    void OnReceiveCallBack(const can_frame &frame);
    std::atomic<int32_t> erpm;
};