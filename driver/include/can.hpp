#pragma once
#include <cstdint>
#include <linux/can.h>
#include <string>

// only support a class for can operation
class Can
{
  public:
    bool Open(std::string dev);
    bool SendFrame(const can_frame &frame);
    const can_frame &GetFrame();
    bool SetFilter(uint32_t id, uint32_t mask);
    // in default loopback is close
    void EnableLoopBack(bool enable);
    ~Can();

  private:
    int soc;
    can_frame recFrame;
};