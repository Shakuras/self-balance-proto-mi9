#pragma once

enum class ErrorCode
{
    NoError = 0,
    OverVoltage = 1,
    UnderVoltage = 2,
    OverAbsoluteCurrent = 3,
    MOSOverTemperature = 4,
    MCUUnderVoltage = 5,
    WatchDogTriggering = 6,
    SPIInterfaceError = 7,
    FlashBroken = 8,
    UCurrentOverPhase = 9,
    VCurrentOverPhase = 10,
    WCurrentOverPhase = 11,
    CurrentInbalance = 12,
    ConfigInFLASHBroken = 13,
    ProgramInFlashBroken = 14
};
enum class ControlMode : unsigned short int
{
    Current = 0,
    Speed = 1,
    Duty = 2,
    AbsolutePosition = 3,
    RelatTargetPosition = 4,
    RelatePosition = 5,
    Brake = 6,
    HandBrake = 7,
    Zero = 8,
    StopZero = 9,
    CurrentClimb = 10,
    Empty = 0xFFFF
};
struct MotorConfig
{
    int address;
    int poleCount;
};