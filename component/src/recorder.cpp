#include "recorder.hpp"

using namespace std;

Recorder::~Recorder()
{
    if (file.is_open())
        Save();
}

bool Recorder::SetPath(std::string path)
{
    file = ofstream(path);
    if (!file.is_open())
    {
        return false;
    }
    return true;
}

void Recorder::SaveInt(int value)
{
    file << value << ',';
}

void Recorder::SaveFloat(float value)
{
    file << value << ',';
}

void Recorder::SaveString(std::string str)
{
    file << str << ',';
}

void Recorder::CreatTitle(std::vector<std::string> titles)
{
    for (string title : titles)
    {
        SaveString(title);
    }
    NewLine();
}

void Recorder::NewLine()
{
    file << endl;
    lines++;
}

void Recorder::Save()
{
    file.close();
}