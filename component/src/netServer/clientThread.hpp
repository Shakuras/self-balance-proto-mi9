#pragma once
#include "netServer.hpp"
#include <string>
#include <thread>
#include <utility>
#include <vector>

class ClientThread
{
  public:
    ClientThread(NetServer *server, int clientSkt);
    void Run();
    void Close();

  private:
    struct MessageBag
    {
        char header;
        std::vector<std::pair<std::string, std::string>> data;
    };
    void Loop();
    MessageBag DecodeMessage(std::string message);
    std::string EncodeMessage(MessageBag bag);
    bool closeFlag;
    std::thread clientThread;
    int clientSkt;
    std::vector<std::string> topics;
    NetServer *server;
};