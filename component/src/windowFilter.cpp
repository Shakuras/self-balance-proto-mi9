#include "windowFilter.hpp"

WindowFilter::WindowFilter(int count)
{
    size = count;
    while (que.size() < size)
    {
        que.push_back(0);
    }
}

void WindowFilter::PushNewItem(float value)
{
    que.push_back(value);
    que.pop_front();
}

float WindowFilter::Result()
{
    float sum = 0;
    for (size_t i = 0; i < size; i++)
    {
        sum += que[i];
    }
    return sum / size;
}