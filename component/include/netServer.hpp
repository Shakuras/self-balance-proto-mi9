#pragma once

#include <string>
#include <map>
#include <thread>
#include <vector>

class ClientThread;

class NetServer
{
  public:
    ~NetServer();
    bool Open(std::string ip, uint16_t port, int maxAccept);
    void SetValue(std::string key, float value);
    void SetValue(std::string key, int value);
    void SetValue(std::string key, std::string value);
    bool GetValue(std::string key, float &value);
    bool GetValue(std::string key, int &value);
    bool GetValue(std::string key, std::string &value);
    float GetValueOrDefault(std::string key, float def);
    int GetValueOrDefault(std::string key, int def);
    std::string GetValueOrDefault(std::string key, std::string def);
    void Close();
    void ChangeFrequent(int updateIntervalMs);

  private:
    std::map<std::string, std::string> valueDic;
    std::vector<ClientThread *> clientThreads;
    std::thread listenThread;
    int serverSkt;
    int updateInterval = 100; // ms
    bool closeServerFlag;
    void Listen();
    void ClientLoop(int clientSkt);
};