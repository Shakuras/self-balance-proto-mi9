#pragma once

class AdditivePID
{
  public:
    AdditivePID(float P = 0, float I = 0, float D = 0);
    void SetP(float value);
    void SetI(float value);
    void SetD(float value);
    // attention: return value has add las ctrl
    float GetControl(float error);
    float GetControlTrick1(float error);
    // attention: return value has add las ctrl
    float GetControlWithClamp(float error, float range);
    void Reset();

  private:
    float P, I, D;
    float lastSecondError = 0;
    float lastError = 0;
    float ctrl = 0;
};