#pragma once
#include <fstream>
#include <string>
#include <vector>

class Recorder
{
  public:
    ~Recorder();
    bool SetPath(std::string path);
    void SaveInt(int value);
    void SaveFloat(float value);
    void SaveString(std::string str);
    void NewLine();
    void CreatTitle(std::vector<std::string> titles);
    void Save();
    int Lines() const { return lines; }

  private:
    std::ofstream file;
    int lines = 0;
};