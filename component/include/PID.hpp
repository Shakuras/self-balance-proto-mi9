#pragma once

class PID
{
  public:
    PID(float P = 0, float I = 0, float D = 0);
    void SetP(float value);
    void SetI(float value);
    void SetD(float value);
    float GetControl(float error, float timeDelta);
    float GetControlWithClamp(float error, float timeDelta, float range);
    float GetControlWithDError(float error, float deltaError, float timeDelta);
    float GetControlWithDerrorAndClamp(float error, float deltaError, float timeDelta, float range);
    void Reset();

  private:
    float P, I, D;
    float errorSum = 0;
    float lastError = 0;
};