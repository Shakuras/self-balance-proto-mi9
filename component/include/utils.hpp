#pragma once

inline float ABSClamp(float value, float range)
{
    value = value < -range ? -range : value;
    value = value > range ? range : value;
    return value;
}

inline bool InABSRange(float value, float range)
{
    return value > -range && value < range;
}

inline bool HasSameSign(float value1, float value2)
{
    return value1 * value2 >= 0;
}