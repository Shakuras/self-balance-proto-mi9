#pragma once
#include <deque>

class WindowFilter
{
public:
    WindowFilter(int count = 1);
    void PushNewItem(float value);
    float Result();

private:
    int size;
    std::deque<float> que;
};