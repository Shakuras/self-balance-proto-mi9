#pragma once
#include <string>
#include <yaml-cpp/yaml.h>
class Config
{
  public:
    struct SerialCfg
    {
        std::string device;
        int baudrate;
        std::string type;
    };
    struct MotorCfg
    {
        int address;
        int maxERPM;
        int pole;
    };
    struct JoystickCfg
    {
        std::string device;
    };
    struct SocketCfg
    {
        std::string ip;
        int port;
    };
    struct PIDParams
    {
        float P;
        float I;
        float D;
    };
    struct AixCfg
    {
        int number;
        bool invert;
        float range;
    };
    /*hardware*/
    SerialCfg IMU;
    SerialCfg MototrBus;
    SerialCfg SerialStick;
    MotorCfg leftMotor;
    MotorCfg rightMotor;
    JoystickCfg joystick;
    SocketCfg socket;
    /*balance*/
    PIDParams gyroPID;
    PIDParams anglePID;
    PIDParams speedPID;
    PIDParams rotatePID;
    float maxAngle;
    float maxOutput;
    float errorAngle;
    float errorRPM;
    float zeroPoint;
    /*control*/
    AixCfg moveAix;
    AixCfg rotateAix;

  public:
    bool LoadFile(std::string path);

  private:
    void LoadPIDParams(PIDParams &param, const YAML::Node &node);
    void LoadMotorParams(MotorCfg &cfg, const YAML::Node &node);
    void LoadSerialParams(SerialCfg &cfg, const YAML::Node &node);
    void LoadAixParams(AixCfg &cfg, const YAML::Node &node);
};