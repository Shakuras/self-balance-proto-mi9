#include <additivePID.hpp>
#include "application.hpp"
#include <iostream>
#include <utils.hpp>
#include <windowFilter.hpp>
#include <serialStick.hpp>

class Net_can_current_APP_tri : public Application
{
    float rpmL = 0;
    float rpmR = 0;
    Recorder recorder;
    NetServer server;
    AdditivePID gyroPID;
    PID anglePID;
    AdditivePID speedPID;
    PID rotatePID;
    WindowFilter filter = WindowFilter(20);
    WindowFilter filter1 = WindowFilter(5);
    SerialStick stick;

    int count = 0;
    int countAngleCtrl = 0;
    int countPrint = 0;
    int countRecord = 0;
    int countServer = 0;

    float deltaMs = 0;
    float avrRPM = 0;
    float speedCtrlAngle = 0;
    float gyroCtrl = 0;
    float ctrl = 0;
    float lastCtrl = 0;

    bool OnTick(Input input) override
    {
        using namespace std;

        // float moveInput = server.GetValueOrDefault("move", 0.0f);
        // float rotateInput = server.GetValueOrDefault("rotate", 0.0f);
        stick.ReadData();
        float moveInput = stick.GetAix(2);
        float rotateInput = stick.GetAix(1);
        input.move = moveInput * config.moveAix.range;
        input.rotate = -rotateInput * config.rotateAix.range;

        // speed control
        deltaMs += state.deltaTimeMs;
        // avrRPM = avrRPM * 0.4 + 0.6 * (rpmL + rpmR) / 2;
        filter.PushNewItem((rpmL + rpmR) / 2);
        avrRPM = filter.Result();
        FrequencyDividerInvoke(20, count, [this, input] {
            float speedError = avrRPM - input.move;
            // speed loop is positive
            speedCtrlAngle = -speedPID.GetControl(speedError);
            speedCtrlAngle = ABSClamp(speedCtrlAngle, config.maxAngle);
            deltaMs = 0;
        });

        // IMU read
        // angle in state forward is negative(-),backward is positive(+)
        IMUState IMUState = device.imu.GetState();
        // now angle forward is positive(+),backward is negative(-)
        float angle = -IMUState.angle[1] + config.zeroPoint;
        float gyro = IMUState.gyro[1];
        float gyroz = IMUState.gyro[2];
        CheckValueRangeAndExit(angle, -config.errorAngle, config.errorAngle, "angle");

        // angle control loop
        // forward is positive(+),backward is negative(-)
        FrequencyDividerInvoke(4, countAngleCtrl, [this, gyro, angle, input] {
            float angleError = angle - speedCtrlAngle;
            // float angleError = angle - input.move;
            gyroCtrl = -anglePID.GetControlWithDError(angleError, gyro, state.deltaTimeMs);
        });

        // gyro control loop
        float gyroError = gyro - gyroCtrl;
        filter1.PushNewItem(gyroError);
        gyroError = filter1.Result();
        ctrl = gyroPID.GetControlTrick1(gyroError);
        ctrl = ABSClamp(ctrl, config.maxOutput);

        float ctrlDelta = ctrl - lastCtrl;
        lastCtrl = ctrl;
        // rotate loop
        float rotateCtrl = rotatePID.GetControl(gyroz - input.rotate, state.deltaTimeMs);
        rotateCtrl = ABSClamp(rotateCtrl, config.maxOutput * 0.2f);

        rpmL = SetMotorAndGet(ctrl - rotateCtrl, device.motorCanLeft);
        rpmR = SetMotorAndGet(ctrl + rotateCtrl, device.motorCanRight);

        Application::Message message{{"rpm", avrRPM},
                                     {"rawRPM", (rpmL + rpmR) / 2},
                                     {"cA", speedCtrlAngle},
                                     {"angle", angle},
                                     /*{"ctrlGyro", gyroCtrl},*/
                                     {"gyro", gyro},
                                     /*{"gyroE", gyroError},*/
                                     /*{"ctrlDelta", ctrlDelta},*/
                                     {"ctrl", ctrl},
                                     {"input", input.move}};

        FrequencyDividerInvoke(5, countPrint, [this, message] { PrintOut(message); });
        FrequencyDividerInvoke(1, countRecord, [this, message] { RecordAll(recorder, message); });
        // FrequencyDividerInvoke(1, countRecord, [this, message] {  ServerUpload(server, message); });
        return true;
    }

    virtual bool Init() override
    {
        if (!stick.InitDevice(config.SerialStick.device, config.SerialStick.baudrate))
        {
            std::cout << "error init serialStick" << std::endl;
            return false;
        }
        // server.Open(config.socket.ip, config.socket.port, 3);
        // server.ChangeFrequent(10);

        gyroPID.SetP(config.gyroPID.P);
        gyroPID.SetI(config.gyroPID.I);
        gyroPID.SetD(config.gyroPID.D);
        gyroPID.Reset();

        anglePID.SetP(config.anglePID.P);
        anglePID.SetI(config.anglePID.I);
        anglePID.SetD(config.anglePID.D);
        anglePID.Reset();

        speedPID.SetP(config.speedPID.P);
        speedPID.SetI(config.speedPID.I);
        speedPID.SetD(config.speedPID.D);
        speedPID.Reset();

        rotatePID.SetP(config.rotatePID.P);
        rotatePID.SetI(config.rotatePID.I);
        rotatePID.SetD(config.rotatePID.D);
        rotatePID.Reset();

        recorder.SetPath("/home/orangepi/dev/self-balance-proto-mi9/data/data.csv");
        bool success = Application::Init();

        SetTickInterval(5);
        std::this_thread::sleep_for(std::chrono::seconds(1));

        return success;
    }

    void Quit() override
    {
        //server.Close();
        recorder.Save();
        Application::Quit();
    }

    float SetMotorAndGet(float value, Motor_Can &motor)
    {
        using namespace std;

        value = ABSClamp(value, config.maxOutput);
        motor.RequestRPM();
        motor.SendHeartBeat();
        motor.SetCurrent(value);
        float rpm = motor.GetRPM();
        CheckValueRangeAndExit(rpm, -config.errorRPM, config.errorRPM, "rpm");
        return rpm;
    }
};

int main()
{
    std::string configFilePath = "/home/orangepi/dev/self-balance-proto-mi9/config/protoMi9_CAN_Current_tri.yaml";
    try
    {
        Net_can_current_APP_tri app;
        app.SimpleStart(configFilePath);
    }
    catch (std::string error)
    {
        std::cout << error << std::endl;
        return -1;
    }
    return 0;
}
