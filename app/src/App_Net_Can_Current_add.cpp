#include "application.hpp"
#include <iostream>
#include <utils.hpp>
#include <windowFilter.hpp>
#include <additivePID.hpp>
class Net_can_current_APP_add : public Application
{
    float rpmL = 0;
    float rpmR = 0;
    Recorder recorder;
    NetServer server;
    AdditivePID anglePID;
    AdditivePID speedPID;
    PID rotatePID;
    WindowFilter filter = WindowFilter(10);
    int count = 0;
    int count2 = 0;

    float avrRPM = 0;
    float ctrl = 0;
    float lastCtrl = 0;
    float speedCtrlAngle = 0;

    bool OnTick(Input input) override
    {
        using namespace std;

        // set input
        float moveInput = server.GetValueOrDefault("move", 0.0f);
        float rotateInput = server.GetValueOrDefault("rotate", 0.0f);
        input.move = moveInput * config.moveAix.range;
        input.rotate = -rotateInput * config.rotateAix.range;

        // speed control
        count++;
        // avrRPM = avrRPM * 0.4 + 0.6 * (rpmL + rpmR) / 2;
        filter.PushNewItem((rpmL + rpmR) / 2);
        avrRPM = filter.Result();
        if (count > 40 && state.loopCount > 200)
        {
            float speedError = avrRPM - input.move;
            // speed loop is positive
            speedCtrlAngle = -speedPID.GetControl(speedError);
            speedCtrlAngle = ABSClamp(speedCtrlAngle, config.maxAngle);
            count = 0;
        }

        // FrequencyDividerInvoke(40, count, [this, &input] {
        //});

        // IMU read
        // angle in state forward is negative(-),backward is positive(+)
        IMUState IMUState = device.imu.GetState();
        // now angle forward is positive(+),backward is negative(-)
        float angle = -IMUState.angle[1] + config.zeroPoint;
        float gyro = IMUState.gyro[1];
        float gyroz = IMUState.gyro[2];
        CheckValueRangeAndExit(angle, -config.errorAngle, config.errorAngle, "angle");

        // angle control loop
        // forward is positive(+),backward is negative(-)
        float angleError = angle - speedCtrlAngle;
        // float angleError = angle - input.move;
        ctrl = anglePID.GetControlWithClamp(angleError, config.maxOutput);
        float ctrlDelta = ctrl - lastCtrl;
        lastCtrl = ctrl;

        float rotateCtrl = rotatePID.GetControl(gyroz - input.rotate, state.deltaTimeMs);
        rotateCtrl = ABSClamp(rotateCtrl, config.maxOutput * 0.2f);
        rpmL = SetMotorAndGet(ctrl - rotateCtrl, device.motorCanLeft);
        rpmR = SetMotorAndGet(ctrl + rotateCtrl, device.motorCanRight);

        Application::Message message{{"rpm", avrRPM}, {"cA", speedCtrlAngle}, {"angle", angle},
                                     {"ctrl", ctrl},  {"input", input.move},  {"d", ctrlDelta}};
        FrequencyDividerInvoke(5, count2, [this, message] { PrintOut(message); });
        RecordAll(recorder, message);
        // ServerUpload(server, message);
        return true;
    }

    virtual bool Init() override
    {
        server.Open(config.socket.ip, config.socket.port, 3);
        server.ChangeFrequent(10);

        anglePID.SetP(config.anglePID.P);
        anglePID.SetI(config.anglePID.I);
        anglePID.SetD(config.anglePID.D);
        anglePID.Reset();

        speedPID.SetP(config.speedPID.P);
        speedPID.SetI(config.speedPID.I);
        speedPID.SetD(config.speedPID.D);
        speedPID.Reset();

        rotatePID.SetP(config.rotatePID.P);
        rotatePID.SetI(config.rotatePID.I);
        rotatePID.SetD(config.rotatePID.D);
        rotatePID.Reset();

        recorder.SetPath("/home/orangepi/dev/self-balance-proto-mi9/data/data.csv");
        bool success = Application::Init();

        SetTickInterval(5);
        return success;
    }

    void Quit() override
    {
        server.Close();
        recorder.Save();
        Application::Quit();
    }

    float SetMotorAndGet(float value, Motor_Can &motor)
    {
        using namespace std;

        value = ABSClamp(value, config.maxOutput);
        motor.RequestRPM();
        motor.SendHeartBeat();
        motor.SetCurrent(value);
        float rpm = motor.GetRPM();
        CheckValueRangeAndExit(rpm, -config.errorRPM, config.errorRPM, "rpm");
        return rpm;
    }
};

int main()
{
    std::string configFilePath = "/home/orangepi/dev/self-balance-proto-mi9/config/protoMi9_CAN_Current.yaml";

    Net_can_current_APP_add app;
    try
    {
        app.SimpleStart(configFilePath);
    }
    catch (std::string error)
    {
        std::cout << error << std::endl;
        return -1;
    }
    return 0;
}
