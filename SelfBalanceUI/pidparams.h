#ifndef PIDPARAMS_H
#define PIDPARAMS_H

#include <QWidget>

namespace Ui
{
class PIDParams;
}

class PIDParams : public QWidget
{
    Q_OBJECT

public:
    explicit PIDParams(QWidget *parent = nullptr);
    ~PIDParams();

private:
    Ui::PIDParams *ui;
};

#endif // PIDPARAMS_H
