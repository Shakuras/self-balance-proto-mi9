import socket
import coder
import time

address = "192.168.10.160"
port = 5000
interval = 1
moveTrack=[(0,0),(1,0),(2,10),(5,0),(2,-10),(5,0)]
rotateTrack=[(0,0),(1,0)]
tracks=[
    (moveTrack,"move")
    (rotateTrack,"rotate")
    ]

def Sample(track:list,time:float)->float:
    for i in range(1,len(track)):
        if track[i][0]>time:
            # (v1-v0)/(t1-t0)*(t-t0)+v0
            return (track[i][1]-track[i-1][1])/(track[i][0]-track[i-1][0])*(time-track[i-1][0])+track[i-1][1];
    return 0

s=socket.socket()
while(True):
    try:
        print("try connect to server...")
        s.connect((address,port))
        break
    except:
        time.sleep(1)

print("connecting to "+address+':'+str(port))

input={}
time=0
for track in tracks:
    input[track[1]]=0

while(True):
    for track in tracks:
        input[track[1]]=Sample(track,time)
    bytes = coder.EncoderWrite(input).encode()
    try:
        s.send(bytes)
    except:
        s.close()
        print("Send fail,maybe server close")
        exit()
    time.sleep(interval)
    time+=interval

    