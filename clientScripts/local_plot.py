import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

filePath=""
data = []
ranges=[]
noRemapRanges=[]
if len(sys.argv)==1:
    sys.exit()
if len(sys.argv)>1:
    data =pd.read_csv(sys.argv[1]);
    ranges = range(0,len(data.columns)-1)
if len(sys.argv)>2:
    ranges = [int(s) for s in sys.argv[2].split(',')]
if len(sys.argv)>3:
    noRemapRanges = [int(s) for s in sys.argv[3].split(',')]

index = np.linspace(0,len(data)-1,len(data)-1)/100
for i in ranges:
    dataLines = data.iloc[1:,i]
    if noRemapRanges.count(i)==0:
        range=max(abs(dataLines))
        if range != 0:
            dataLines/=range
    line, = plt.plot(index,dataLines,label=data.columns.values[i])
    #line.set_dashes([1,1,1,1])
plt.grid()
plt.legend()
plt.show()
