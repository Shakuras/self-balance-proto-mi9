import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

config=[]
if len(sys.argv)>2:
    data = pd.read_csv(sys.argv[1])
    pairs = [s for s in sys.argv[2].split(';')]
    for pair in pairs:
        values = pair.split(',') 
        config+=[(values[0],int(values[1]))];

index = np.linspace(0,(len(data)-1)/200,len(data)-1)
for cfg in config:
    dataLines = data.loc[:,cfg[0]]
    dataLines/=cfg[1]
    line, = plt.plot(index,dataLines[1:],label=cfg[0])

plt.grid()
plt.legend()
plt.show()
