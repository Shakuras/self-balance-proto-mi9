def EncodeRequest(keys:list)->str:
    message="@R="
    for key in keys:
        message+=key+':1,'
    message= message[:-1]+'\n'
    return message

def EncoderWrite(dict:dict)->str:
    message = "@W="
    for pair in dict.items():
        message+= pair[0]+':'+str(pair[1])+','
    message= message[:-1]+'\n'
    return message

def DecoderResponse(response:str)->dict:
    realResponse = response[3:-1]
    pairs = realResponse.split(',')
    dict ={}
    for pair in pairs:
        key,val = pair.split(':')
        dict.update({key:float(val)})
    return dict

def EncoderResponse(dict:dict)->str:
    message = "@B="
    for pair in dict.items():
        message+= pair[0]+':'+str(pair[1])+','
    message= message[:-1]+'\n'
    return message

def EncoderHeartBeat()->bytes:
    return "@H=12".encode()


#input = {'move':'12345.123','rotate':'123'}
#mes = "@B=var:12,acc:13,rpm:90\n"
#mes = mes.encode()
#mes = DecoderResponse(mes)
#mes = EncoderWrite(input)
#mes = EncodeRequest(["var","acc","rpm"])
#print(mes)