from math import floor, sqrt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import glob


#files=["data.csv","data2.csv","data2.csv","data2.csv","data2.csv"]
files=glob.glob('*.csv')
print(files)

def PlotSubplot(str:str):
    data = pd.read_csv(str)
    n = len(data.columns)-1
    index = np.linspace(0,len(data)-1,len(data)-1)/100
    for i in range(0,n):
        dataLines = data.iloc[1:,i]
        dataRange=max(abs(dataLines))
        if dataRange != 0:
            dataLines/=dataRange
            print(data.columns.values[i])
        line, = plt.plot(index,dataLines,label=data.columns.values[i])
        #line.set_dashes([1,1,1,1])
    plt.title(str)
    plt.legend(prop = {'size':6})

length = len(files)
w = floor(sqrt(length)+1)
n = floor(length/w+0.99)
for i in range(0,length):
    plt.subplot(w,n,i+1)
    PlotSubplot(files[i])
plt.show()
