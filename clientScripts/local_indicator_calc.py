import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def iqr_outliers(array):
    out=[]
    q1 = array.quantile(0.25)
    q3 = array.quantile(0.75)
    iqr = q3-q1
    Lower_tail = q1 - 1.5 * iqr
    Upper_tail = q3 + 1.5 * iqr
    for i in array:
        if i < Upper_tail and i > Lower_tail:
            out.append(i)
    return out

data = pd.read_csv("data_empty_1.csv")
gyro = data.loc[1:,"gyro"]
current = data.loc[1:,"ctrl"]
indicator=gyro/current
index1 = np.linspace(0,(len(data)-1)/200,len(data)-1)
#plt.plot(index,gyro,label="gyro")
#plt.plot(index,current/300,label="ctrl")
ind_clean = iqr_outliers(indicator)
index2 = np.linspace(0,(len(data)-1)/200,len(ind_clean))
#plt.plot(index1,indicator,label="indicator")
plt.plot(index2,ind_clean,label="ind_clean")
print("average indicator:")
print(np.mean(indicator))
print("indicator variance:")
print(np.var(indicator))
print("average indicator clean:")
print(np.mean(ind_clean))
print("indicator clean variance:")
print(np.var(ind_clean))
plt.grid()
plt.legend()
plt.show()
