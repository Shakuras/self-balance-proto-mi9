# SelfBalanceProtoMi9

#### 介绍
基于小米平衡车硬件，自行搭建平衡车原型机,用以验证自平衡相关算法。
![平衡车](./res/selfblance.jpg)
#### 依赖
+ LibModBus 
+ Yaml-cpp
#### 文件架构
+ `driver` 存放硬件驱动函数
    + Serial
    + Can
    + IMU
    + MotorCan
    + Motor485
    + Joystick
+ `component` 存放软件算法和调试功能模块
    + IPC
    + Socket
    + Recoder
    + Utils
+ `appliaction` 存放应用及系统运行流程
    + FrameWork
    + Config load
+ `clientScript` 客户端控制调试脚本
    + Runtime plot
    + Remote ctrl
+ `test` 存放驱动和组件的功能测试程序
+ `config` 存放配置文件